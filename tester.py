#!/usr/bin/python
import serial, time, sys, getopt, argparse
from collections import defaultdict
import robel_parser as rbl
import itertools, sys, operator

import numpy as np
import pylab as pl

results = defaultdict(int)
use_serial = False
tally = True
tally_bin = defaultdict(lambda: 0)

def display(yip, r, tally=False):
    if type(yip) is not list:
        tokens = rbl.parse(yip) # if yip is expression
    else:
        tokens = yip    # if yip is parsed/tokenized input
    #if tally:
    #   tally_bin = defaultdict(lambda: 0)

    print "Tallying: ... \n"
    for i in range(r):      
        
        word = rbl.eval(tokens)
        word = word.replace(" ","")
        #print "Displaying %s>>" % (word.replace(" ",""))
        results[word] += 1

    sorted_results = sorted(results.items(), key=lambda x: (x[1], len(x[0]), x[0]), reverse=True)

    for k, v in sorted_results:
        print "'%s': %d times" % (k, v)
    print sorted_results    
    graphitout(sorted_results)
    # if tally:
    #   for k, v in tally_bin.iteritems():
    #       print "'%s': %s times" % (k, v)

def graphitout(data, **kwargs):
    # Ref: http://stackoverflow.com/questions/16892072/histogram-in-pylab-from-a-dictionary
    R = np.arange(len(data))
    if len(data) > 0:
        if type(data) == dict:
            pl.bar(R, data.values(), align='center')
            pl.xticks(R, data.keys())
            ymax = max(data.values()) + 10
            pl.ylim(0, ymax)
            pl.show()
        elif type(data) == list:
            x, y = zip(*data)
            pl.bar(R, y, align='center')
            pl.xticks(R, x)
            ymax = max(y) + 10
            pl.ylim(0, ymax)
            pl.show()

def main(argv):
    rep = 1
    #expression = '(a & ((b + c) & ((c + f)*)))'
    expression = '((b + c) *)'
    # ref: http://www.tutorialspoint.com/python/python_command_line_arguments.htm
    
    parser = argparse.ArgumentParser(description="RoBeL Parser")
    parser.add_argument('-r', metavar='R', type=int, nargs=1, default=[1], help='number of times the expression will be evaluated (default=1)')
    parser.add_argument('-e', metavar='E', type=str, nargs=1, help='expression', default=[expression])
    parser.add_argument('--tally', help='tally output symbols (default: False).', action='store_true')
    parser.add_argument('-o', help='Output to serial (default: False).', action='store_true')
    
    args = vars(parser.parse_args(argv))
    expression = args['e'][0]
    rep = args['r'][0]
    tally = args['tally']
    use_serial = args['o']
    rep = 1000
    if rep == 1:
        rep_text = "once"
    else:
        rep_text = "%s times" % rep
    print "Will evaluate expression: \'%s\' %s." % (expression, rep_text)
    display(expression, rep, tally)

if __name__ == "__main__":
    main(sys.argv[1:])