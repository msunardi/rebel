#!/usr/bin/python
import serial, time, sys, getopt, argparse
from collections import defaultdict
import robel_parser as rbl

import serial.tools.list_ports as lp

DEFAULT_SERIAL = '/dev/tty.usbmodemfd121'

port = ''

preferred_ports = ['/dev/ttyACM0', '/dev/ttyACM1', '/dev/tty.usbmodemfa131', 'COM3']

ports = list(lp.comports())
use_serial = False
tally = False
tally_bin = defaultdict(lambda: 0)

for p in ports:
    print p
    if p[1] in preferred_ports or p[0] in preferred_ports: # in windows, port name in p[0]
        port = p[0]
        print "Found one of the preferred ports: %s" % (port)
        break
else:
    port = DEFAULT_SERIAL

ser = None

if use_serial:
	try:
		print "Trying to connect to port: %s ..." % (port),
		ser = serial.Serial(port, 9600)
	except (serial.serialutil.SerialException, OSError):
		use_serial = False
		print "Unable to connect: Port not available/exist."
		#sys.exit()

	if ser: print "Connected!"
else:
	print "Not using serial."

def display(yip, r=5, tally=False):
 	if type(yip) is not list:
 		tokens = rbl.parse(yip)	# if yip is expression
 	else:
 		tokens = yip	# if yip is parsed/tokenized input
 	#if tally:
 	#	tally_bin = defaultdict(lambda: 0)

 	for i in range(r):
 		word = rbl.eval(tokens)
 		print "Displaying %s>>" % (word)
 		#if use_serial and ser: ser.write('w')
 		if use_serial: time.sleep(2)
 		for c in word.replace('\n','').replace('\r','').split( ):
 			
 			if use_serial and ser: 
 				print "Printing...%s" % (c)
 				ser.write(c)
 				time.sleep(1.5)
 			tally_bin[c] += 1
	if tally:
		for k, v in tally_bin.iteritems():
			print "'%s': %s times" % (k, v)

def main(argv):
	rep = 1
	#expression = '(a & ((b + c) & ((c + f)*)))'
	expression = '((a & (b + c)) *)'
	# ref: http://www.tutorialspoint.com/python/python_command_line_arguments.htm
	"""try:
		#opts, args = getopt.getopt(args, "he:")
		opts, args = getopt.getopt(argv, "her:")
	except getopt.GetoptError:
		print 'robel_serial.py -e <expression>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'robel_serial.py -e <expression>'
			sys.exit()
		elif opt in ("-e"):
			expression = arg
		elif opt in ("-r"):
			rep = arg
	"""
	parser = argparse.ArgumentParser(description="RoBeL Parser")
	parser.add_argument('-r', metavar='R', type=int, nargs=1, default=[1], help='number of times the expression will be evaluated (default=1)')
	parser.add_argument('-e', metavar='E', type=str, nargs=1, help='expression', default=[expression])
	parser.add_argument('--tally', help='tally output symbols (default: False).', action='store_true')
	parser.add_argument('-o', help='Output to serial (default: False).', action='store_true')
	
	args = vars(parser.parse_args(argv))
	expression = args['e'][0]
	rep = args['r'][0]
	tally = args['tally']
	use_serial = args['o']
	
	if rep == 1:
		rep_text = "once"
	else:
		rep_text = "%s times" % rep
	print "Will evaluate expression: \'%s\' %s." % (expression, rep_text)
	display(expression, rep, tally)

if __name__ == "__main__":
    main(sys.argv[1:])
